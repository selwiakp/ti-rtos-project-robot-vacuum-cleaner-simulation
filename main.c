/*
 *  ======== main.c ========
 */

#include <xdc/std.h>

#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>

#include <ti/sysbios/knl/Task.h>

#include "F28x_Project.h"

#include "gui.h"

#include <xdc/cfg/global.h>

void state_idle(struct gui_t *self);
void state_preparing(struct gui_t *self);
void state_cleaning(struct gui_t *self);
void state_charging(struct gui_t *self);
bool bStart = FALSE;

void dolej(struct gui_t *self)
{
    self->indicator.plyn = 100;
}

void oproznij(struct gui_t *self)
{
    self->indicator.kurz = 0;
}

void start(void)
{
    bStart = TRUE;
}

void stop(void)
{
    state_idle(&gui);
}

void clock(void)
{
    if(bStart)
    {
        state_preparing(&gui);
    }
}

void buttons(void)
{
    GUI_BUTTON(gui.control.button_stop, state_idle(&gui));
    GUI_BUTTON(gui.control.button_oproznijkurz, oproznij(&gui));
    GUI_BUTTON(gui.control.button_dolejplyn, dolej(&gui));
    GUI_BUTTON(gui.control.button_start, start());
}

void state_idle(struct gui_t *self)
{
    bStart = FALSE;
    self->indicator.message = "IDLE";
    self->indicator.odkurzanie = 0;
    self->indicator.mycie = 0;
    self->indicator.ladowanie = 0;
    self->indicator.stacja = 1;
}

void state_preparing(struct gui_t *self)
{
   if(odkurzanie == (self->control.mode))
    {
        self->indicator.odkurzanie = 1;
        self->indicator.mycie = 0;
        self->indicator.message = "ODKURZAM";
        self->indicator.stacja = 0;
        self->indicator.ladowanie = 0;
        state_cleaning(&gui);
    }
    else if(mycie == (self->control.mode))
    {
        self->indicator.mycie = 1;
        self->indicator.message = "ZMYWAM";
        self->indicator.stacja = 0;
        self->indicator.odkurzanie = 0;
        self->indicator.ladowanie = 0;
        state_cleaning(&gui);
    }
    else if(ladowanie == (self->control.mode))
    {
        self->indicator.ladowanie = 1;
        self->indicator.mycie = 0;
        self->indicator.message = "LADOWANIE";
        self->indicator.stacja = 1;
        self->indicator.odkurzanie = 0;
        self->indicator.ladowanie = 0;
        state_charging(&gui);
    }
}

void state_cleaning(struct gui_t *self)
{
    if((self->indicator.bateria) > 0)
    {
        if((odkurzanie == (self->control.mode)) && ((self->indicator.kurz) < 100))
        {
            self->indicator.kurz += 10;
            self->indicator.bateria -= 10;
        }
        else if((mycie == (self->control.mode)) && ((self->indicator.plyn) > 0))
        {
            self->indicator.plyn -= 20;
            self->indicator.bateria -= 10;
        }
    }
    else
    {
        bStart = FALSE;
        state_idle(&gui);
    }
}

void state_charging(struct gui_t *self)
{
    if((self->indicator.bateria) < 100)
    {
    self->indicator.ladowanie = 1;
    self->indicator.stacja = 1;
    self->indicator.odkurzanie = 0;
    self->indicator.mycie = 0;
    self->indicator.bateria += 20;
    }
    else
    {
       bStart = FALSE;
       state_idle(&gui);
    }
}


/*
 *  ======== main ========
 */
Int main(){
    /*
     * use ROV->SysMin to view the characters in the circular buffer
     */
    System_printf("enter main()\n");
    gui_init(&gui);

    BIOS_start();    /* does not return */
    return(0);
}
