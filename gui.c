/*
 * gui.c
 */

#include "gui.h"

#define STRING_SIZE 100

struct gui_t gui;

char message[STRING_SIZE] = "witaj";

void gui_init(struct gui_t *self)
{
    self->control.button_start = 0;
    self->control.button_stop = 0;
    self->control.button_oproznijkurz = 0;
    self->control.button_dolejplyn = 0;
    self->control.mode = odkurzanie;

    self->indicator.message = message;
    self->indicator.odkurzanie = 0;
    self->indicator.mycie = 0;
    self->indicator.ladowanie = 0;
    self->indicator.bateria = 100;
    self->indicator.plyn = 100;
    self->indicator.kurz = 0;
    self->indicator.stacja = 1;
}
