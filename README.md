# Ti-RTOS project robot vacuum cleaner simulation

Ti-RTOS project robot vacuum cleaner simulation developed for F28377S, using Code Composer Studio (v. 10.4.0.00006) and GUI Composer.

## Getting started

Copy vacuum_cleaner_simulation folder to dropins-gc by default it is in location C:\ti\ccs1020\ccs\eclipse\dropins-gc.
Compile code in Code Composer Studio. Open view GUI composer and select vacuum_cleaner_simulation.
Run code on your board.
