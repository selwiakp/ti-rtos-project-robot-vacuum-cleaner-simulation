/*
 * gui.h
 */

#ifndef GUI_H_
#define GUI_H_

#define GUI_BUTTON(button, action) if(button){button = 0; action;}

enum gui_options_t
{
    odkurzanie,
    mycie,
    ladowanie
};
struct gui_controls_t
{
    int button_start;
    int button_stop;
    int button_oproznijkurz;
    int button_dolejplyn;
    enum gui_options_t mode;
};

struct gui_indicators_t
{
    char *message;
    unsigned char odkurzanie;
    unsigned char mycie;
    unsigned char ladowanie;
    unsigned char stacja;
    int bateria;
    int plyn;
    int kurz;
};

struct gui_t
{
    struct gui_controls_t control;
    struct gui_indicators_t indicator;
};


extern struct gui_t gui;

void gui_init(struct gui_t *self);

#endif /* GUI_H_ */
